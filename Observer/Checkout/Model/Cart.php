<?php
declare(strict_types = 1);
namespace Johnadavies\Georestrict\Observer\Checkout\Model;

use Magento\Framework\Exception\LocalizedException;

class Cart implements \Magento\Framework\Event\ObserverInterface
{

    /**
     * @var \Johnadavies\Georestrict\Helper\Data
     */
    private $helper;

    /**
     * @var \Johnadavies\Georestrict\Model\LocationInterface
     */
    private $location;

    /**
     * @var \Magento\Framework\App\Request\DataPersistorInterface
     */
    private $dataPersistor;

    /**
     * @var \Magento\Framework\HTTP\PhpEnvironment\RemoteAddress
     */
    private $remoteAddress;


    /**
     * Product constructor.
     * @param \Johnadavies\Georestrict\Helper\Data $helper
     */
    public function __construct(
        \Johnadavies\Georestrict\Helper\Data $helper,
        \Johnadavies\Georestrict\Model\LocationInterface $location,
        \Magento\Framework\App\Request\DataPersistorInterface $dataPersistor,
        \Magento\Framework\HTTP\PhpEnvironment\RemoteAddress $remoteAddress
    )
    {
        $this->helper = $helper;
        $this->location = $location;
        $this->dataPersistor = $dataPersistor;
        $this->remoteAddress = $remoteAddress;
    }


    /**
     * @param \Magento\Framework\Event\Observer $observer
     * @throws LocalizedException
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $product=$observer->getProduct();
        if ($this->helper->isModuleEnabled()) {
            if (!$this->canUserBuyProduct($product)) {
                throw new LocalizedException(__($this->helper->getDefaultErrorMessage(),$this->getUsersCountry()));
            }
        }
    }

    /**
     * Get the countries in which the product can be bought
     * @param Product $product
     * @return array|bool
     */
    private function getProductAvailabilityCountries($product)
    {
        $countries = $product->getProductAllowedCountries();//Need to replace this with correct method call

        if (!is_null($countries)) {
            $countries = explode(',', $countries);
        } else {
            $countries = false;
        }
        return $countries;
    }

    /**
     * Returns the users country code. If they dont have one it looks it up from
     * the Location provider
     * @return string
     */
    private function getUsersCountry()
    {
        if (!$this->dataPersistor->get('johnadavies_geolocation_users_country')) {
            $countrycode = $this->location->getCountryCode($this->remoteAddress->getRemoteAddress());
            $this->dataPersistor->set('johnadavies_geolocation_users_country', $countrycode);
        } else {
            $countrycode = $this->dataPersistor->get('johnadavies_geolocation_users_country');
        }
        return $countrycode;
    }


    /**
     * @param Product $product
     * @return bool
     */
    private function canUserBuyProduct($product)
    {
        $userCountryCode = $this->getUsersCountry();
        $productCountryCodes = $this->getProductAvailabilityCountries($product);
        $canBuy = true;
        if ($productCountryCodes) {
            if (in_array($userCountryCode, $productCountryCodes)) {
                $canBuy = false;
            }
        }
        return $canBuy;
    }

}