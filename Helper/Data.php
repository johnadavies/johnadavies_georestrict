<?php
/**
 * Created by PhpStorm.
 * User: John
 * Date: 14/12/2020
 * Time: 21:46
 */

namespace Johnadavies\Georestrict\Helper;

use Magento\Store\Model\ScopeInterface;

class Data extends \Magento\Framework\App\Helper\AbstractHelper
{

    const XML_PATH_PRODUCTRESTRICTION = 'johnadavies_georestrict_general/';

    /**
     * @param $field
     * @param null $storeId
     * @return string
     */
    public function getConfigValue($field, $storeId = null):string
    {
        return (string)$this->scopeConfig->getValue(
            $field, ScopeInterface::SCOPE_STORE, $storeId
        );
    }

    /**
     * @param $code
     * @param null $storeId
     * @return string
     */
    public function getGeneralConfig($code, $storeId = null):string
    {
        return (string)$this->getConfigValue(self::XML_PATH_PRODUCTRESTRICTION . 'general/' . $code, $storeId);
    }


    /**
     * @return bool
     */
    public function isModuleEnabled():bool
    {
        return (bool)$this->getGeneralConfig('enable');
    }

    /**
     * @return string
     */
    public function getDefaultErrorMessage()
    {
        return $this->getGeneralConfig('default_error');
    }



}