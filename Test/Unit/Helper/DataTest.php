<?php

namespace Johnadavies\Georestrict\Test\Unit\Helper;

class DataTest extends  \PHPUnit\Framework\TestCase {


    protected $_objectManager;
    protected $_data;

    public function tearDown() {

    }

    
    public function setUp() {
        $this->objectManagerHelper = new \Magento\Framework\TestFramework\Unit\Helper\ObjectManager($this);
        $className = \Johnadavies\Georestrict\Helper\Data::class;
        $arguments = $this->objectManagerHelper->getConstructArguments($className);

        $context = $arguments['context'];
        $this->scopeConfigMock = $context->getScopeConfig();

        $this->_data = $this->objectManagerHelper->getObject($className, $arguments);

    }


    public function testModuleDisableOption()
    {
        $this->assertTrue(is_bool($this->_data->isModuleEnabled()));
    }

    /**
     * testGetDefaultErrorMessage
     */
    public function testGetDefaultErrorMessage()
    {
        $this->assertTrue(is_string($this->_data->getDefaultErrorMessage()));
    }





}