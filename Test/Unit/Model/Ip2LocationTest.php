<?php

namespace Johnadavies\Georestrict\Test\Unit\Model;

class Ip2LocationTest extends  \PHPUnit\Framework\TestCase {


    protected $_objectManager;
    protected $_ip2location;

    public function tearDown() {

    }


    public function setUp() {
        $objectManager = new \Magento\Framework\TestFramework\Unit\Helper\ObjectManager($this);
        $this->_ip2location = $objectManager->getObject('\Johnadavies\Georestrict\Model\Ip2Location');
    }

    public function testGetCountryCodeInvalidIp()
    {
        try{
            $this->_ip2location->getCountryCode('fff');
            $this->assertTrue(false);
        }catch (\Exception $e)
        {
            $this->assertTrue($e->getMessage()=="IP Address Not Valid");
        }
    }

    public function testGetCountryCodeValidIp()
    {
       $this->assertTrue($this->_ip2location->getCountryCode('52.4.227.204'));
    }


   





}