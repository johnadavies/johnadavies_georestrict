<?php
/**
 * Created by PhpStorm.
 * User: John
 * Date: 14/12/2020
 * Time: 23:28
 */

declare(strict_types = 1);

namespace Johnadavies\Georestrict\Model;

/**
 * Class Ip2Location
 * @package Johnadavies\Georestrict\Model
 */
class Ip2Location implements \Johnadavies\Georestrict\Model\LocationInterface
{

    /**
     * @todo -replace "key" with actual key and build URL properly
     */
    const API_REQUEST_URI = "https://api.ip2location.com/v2/?addon=continent&lang=zh-cn&key=demo";

    private $_httpClientFactory;

    public function __construct(
        \Magento\Framework\HTTP\ZendClientFactory $httpClientFactory
    )
    {
        $this->_httpClientFactory = $httpClientFactory;
    }

    /**
     * @param string $ipaddress
     * @return string
     * @throws \Exception
     */
    public function getCountryCode(string $ipaddress)
    {
        if (!filter_var($ipaddress, FILTER_VALIDATE_IP)) {
            throw new \Exception('IP Address Not Valid');
        }
        $client = $this->_httpClientFactory->create();
        $client->setUri(self::API_REQUEST_URI . "&ip=" . $ipaddress);
        $client->setMethod(\Zend_Http_Client::GET);

        $response = $client->request();
        $countryCode = "UNKNOWN";
        if ($response->getStatus() == "200") {
            $body = json_decode($response->getBody());
            if (isset($body->country_code)) {
                $countryCode = $body->country_code;
            }
        }
        return $countryCode;
    }




}