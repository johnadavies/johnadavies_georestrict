<?php
/**
 * Created by PhpStorm.
 * User: John
 * Date: 15/12/2020
 * Time: 21:38
 */
namespace Johnadavies\Georestrict\Model;

interface LocationInterface
{

    /**
     * @param string $ipaddress
     * @return bool
     * @throws \Exception
     */
    public function getCountryCode(string $ipaddress);
}